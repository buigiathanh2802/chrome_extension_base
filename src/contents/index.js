import DefaultContent from "./default";
import GoogleContent from "./www.google.com";

const ContentScript = () => {
    return (
        <>
            <DefaultContent />

            {/*Content run in a website*/}
            {/* ================= Example ================= */}
            {(window.location.origin === "https://www.google.com") && <GoogleContent />}
        </>
    )
}

export default ContentScript